FROM ubuntu:22.04

# Needed as we are not using an app.main folder for the fastapi
ENV MODULE_NAME=ska_cicd_training_pipeline_machinery.main

# no questions, please
ENV DEBIAN_FRONTEND=noninteractive 

# install common tools
RUN apt update && apt install -y apt-transport-https curl ca-certificates software-properties-common build-essential gawk vim

# install python and poetry
RUN apt update && apt install -y python3.10 git python3.10-venv python3-pip
RUN curl -sSL https://install.python-poetry.org | POETRY_HOME=/opt/poetry python3 -
RUN ln -s /opt/poetry/bin/poetry /usr/local/bin/poetry 

# Copy poetry.lock* in case it doesnt exist in the repo
COPY pyproject.toml poetry.lock* ./

# Install runtime dependencies
RUN poetry install --only main --no-root

# copy application
COPY ./src /app