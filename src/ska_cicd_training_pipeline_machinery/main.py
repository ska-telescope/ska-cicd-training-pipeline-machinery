"""Simple FASTAPI App for workshop purposes

Returns:
    None
"""

from fastapi import FastAPI

app = FastAPI()


@app.get("/")
async def root():
    """Example root path for hello world

    Returns:
        {"message": "Hello World"} message
    """
    return {"message": "Hello World"}
